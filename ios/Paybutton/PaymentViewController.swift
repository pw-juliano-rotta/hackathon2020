//
//  ViewController.swift
//  Paybutton
//
//  Created by Rotta, Juliano on 17/08/2020.
//  Copyright © 2020 Payworks. All rights reserved.
//

import UIKit
import common
import mpos_ui

class PaymentViewController: UIViewController {
    let mposUi = MPUMposUi.initialize(with: .MOCK, merchantIdentifier: "aaa", merchantSecret: "aa")
    
    @IBOutlet weak var amountTextField: CurrencyField!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    var amount: NSDecimalNumber {
        get {
            return NSDecimalNumber(string: amountTextField.text)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionTextField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        amountTextField.becomeFirstResponder()
    }
    
    func startTransaction() {
        let transactionParameters = MPTransactionParameters.charge(withAmount: amount, currency: .GBP, optionals: nil)
        mposUi.configuration.terminalParameters = MPAccessoryParameters.tcpAccessoryParameters(with: .verifoneVIPA, remote: "192.168.0.111", port: 8090, optionals: nil)
        
        let paymentView = mposUi.createTransactionViewController(with: transactionParameters) { (view, transactionResult, transaction) in
            print(transactionResult)
            self.dismiss(animated: true)
        }
        
        let navigationController = UINavigationController.init(rootViewController: paymentView)
        present(navigationController, animated: true)
    }
}

extension PaymentViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        startTransaction()
        return true
    }
}
