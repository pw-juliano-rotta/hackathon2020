Pod::Spec.new do |s|

  s.name         = "NewPaybutton"
  s.version      = "0.0.1"
  s.license		 = "private"
  s.summary		 = 'Integration library for accessories and comlinks submodules'
  s.homepage     = 'https://bitbucket.org/payworks/io.payworks.mpos.ios'
  s.author       = { 'Juliano ROtta' => 'juliano.rotta@payworks.com' }
  s.platform     = :ios, "10.0"
  s.requires_arc = true
  s.default_subspec = 'sdk'  
  s.swift_version = '5.0'
  s.source =  { :path => "" }
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES' }
  s.static_framework = true

    s.subspec 'sdk' do |sdk|
        sdk.source_files =  "NewPaybutton/**/*.{h,m,swift}" 
        sdk.dependency 'common'
        sdk.dependency 'payworks', '~> 2.41.0'
    end
end
