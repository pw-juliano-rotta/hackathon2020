//
//  MposIos.swift
//  NewPaybutton
//
//  Created by Rotta, Juliano on 18/08/2020.
//  Copyright © 2020 Payworks. All rights reserved.
//

import common
import UIKit
import mpos_core

public class MposUI {
    let merchantIdentifier: String
    let merchantSecret: String
    let actor: Actor
    
    public init(merchantIdentifier: String, merchantSecret: String) {
        self.merchantIdentifier = merchantIdentifier
        self.merchantSecret = merchantSecret
        
        let provider = MPMpos.transactionProvider(for: .TEST, merchantIdentifier: merchantIdentifier, merchantSecretKey: merchantSecret)
        self.actor = Actor(sdk: IosSdkGateway(transactionProvider: provider), reducer: StateReducer())
        self.actor.onStateChange = { state in
            print(state)
        }
    }
    
    public func createSummary(transactionId: String, completion: ((String) -> Void)?) {
        self.actor.sendRequest(request: SdkGateway.SdkRequestGetTransaction(transactionId: transactionId))
    }
    
    public func startTransaction(subject: String, amount: Double, completion: ((String) -> Void)?) {
        self.actor.sendRequest(request: SdkGateway.SdkRequestStartTransaction(subject: subject, amount: amount))
    }
}
