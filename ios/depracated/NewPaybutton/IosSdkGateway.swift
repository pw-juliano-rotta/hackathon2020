//
//  IosSdk.swift
//  NewPaybutton
//
//  Created by Rotta, Juliano on 18/08/2020.
//  Copyright © 2020 Payworks. All rights reserved.
//

import common
import mpos_core

class IosSdkGateway: SdkGateway {
    let transactionProvider: MPTransactionProvider
    
    public init(transactionProvider: MPTransactionProvider) {
        self.transactionProvider = transactionProvider
    }
    
    public override func getTransaction(transactionId: String) {
        transactionProvider.transactionModule.lookupTransaction(withTransactionIdentifier: transactionId) { (transaction, error) in
            self.offer(response: SdkGateway.SdkResponseGetTransactionCompleted(information: transaction?.identifier ?? "Not found"))
        }
    }
    
    override func charge(subject: String, amount: Double) {
        let accessoryParameters = MPAccessoryParameters.tcpAccessoryParameters(with: .verifoneVIPA, remote: "192.168.0.111", port: 8090, optionals: nil)
        let transactionParameters = MPTransactionParameters.charge(withAmount: NSDecimalNumber(value: amount), currency: MPCurrency.GBP, optionals: nil)
        
        transactionProvider.startTransaction(with: transactionParameters, accessoryParameters: accessoryParameters, registered: { (process, transaction) in
            self.offer(response: SdkGateway.SdkResponseTransactionUpdated(information: "onTransactionRegistered", abortable: process.canBeAborted()))
        }, statusChanged: { (process, transaction, details) in
            self.offer(response: SdkGateway.SdkResponseTransactionUpdated(information: "onTransactionStatusChanged", abortable: process.canBeAborted()))
        }, actionRequired: { (process, transaction, actions, actionSupport) in
            self.offer(response: SdkGateway.SdkResponseTransactionUpdated(information: "onTransactionActionRequired", abortable: process.canBeAborted()))
        }) { (process, transaction, processDetails) in
            self.offer(response: SdkGateway.SdkResponseTransactionCompleted(information: "onTransactionCompleted"))
        }
    }
}


//
//extension MPTransaction {
//    func toDetail() -> TransactionDetail? {
//        guard let id = self.identifier else {
//            return nil
//        }
//        return TransactionDetail(id: id, status: self.status.toString())
//    }
//}
//
//extension MPTransactionStatus {
//    func toString() -> String {
//        switch self {
//        case .approved:
//            return "Approved"
//        default:
//            return "Not Approved"
//        }
//    }
//}
//
//extension Error {
//    func toDetail() -> ErrorDetail {
//        return ErrorDetail(errorMessage: self.localizedDescription)
//    }
//}
