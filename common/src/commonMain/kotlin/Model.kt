package io.payworks.mpos.ui.common

import io.payworks.mpos.ui.common.SdkGateway.SdkRequest.*
import io.payworks.mpos.ui.common.SdkGateway.SdkResponse.*

sealed class ViewState {
    object Login : ViewState()
    object LoggingIn : ViewState()
    data class Logged(val applicationName:String, val merchantIdentifier:String, val merchantSecretKey:String) : ViewState()
    data class Ready(val applicationName:String, val merchantIdentifier:String, val merchantSecretKey:String) : ViewState()
    data class TransactionList(val list: List<Transaction>) : ViewState()
}

sealed class Effect {
    data class LoginRequestedEffect(val request: Login) : Effect()
    data class LoginSuccessEffect(val applicationName:String, val merchantIdentifier:String, val merchantSecretKey:String) : Effect()
    object LoginFailEffect : Effect()
}

data class Transaction(val cardType: Int, val amount: String, val date: String, val status: Int)