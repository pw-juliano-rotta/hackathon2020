package io.payworks.mpos.ui.common

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.JsonSerializer
import io.ktor.client.features.json.defaultSerializer
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.url

class Api {
    private val client = HttpClient() {
        install(JsonFeature) {
            serializer = defaultSerializer()
        }
    }

    suspend fun auth(user: String, pass: String): LoginResponse {
        return client.post <LoginResponse>(
            scheme = "https",
            host = "services.test.pwtx.info",
            path = "accounts/authenticate?applicationIdentifier=barclaycard.terminal&deviceIdentifier=payworks.terminal.ios.9c6283096625aacd072c3baa71118b5068d60f949937504d01010779497d806e&profileIdentifier=${user}&profileSecret=${pass}&tokenType=APP&content-type=application/x-www-form-urlencoded"
        )
    }

    suspend fun test(): String {
        return client.get {
            url("https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22")
        }
    }

    companion object {
        private const val baseUrl = "https://services.test.pwtx.info"
    }

    data class LoginResponse(
        val status: String,
        val data: Data
    )


    data class Data(
        val profileIdentifier: String,
        val profileToken: String,
        val validUntil: String,
        val username: String,
        val applicationName: String,
        val merchantIdentifier: String,
        val merchantSecretKey: String,
        val roles: List<String>,
        val country: String,
        val workflows: List<String>,
        val schemes: List<String>
    )
}