package io.payworks.mpos.ui.common

import io.payworks.mpos.ui.common.ViewState.*

class StateReducer {
    fun reduce(viewState:ViewState, effect: Effect) = when(effect) {
        is Effect.LoginRequestedEffect -> LoggingIn
        is Effect.LoginSuccessEffect -> Logged(effect.applicationName, effect.merchantIdentifier, effect.merchantSecretKey)
        is Effect.LoginFailEffect -> Login
    }
}