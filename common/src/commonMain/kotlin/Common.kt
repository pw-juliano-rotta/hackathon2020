package io.payworks.mpos.ui.common

import kotlinx.coroutines.CoroutineScope

expect fun platformName(): String

fun createApplicationScreenMessage() : String {
    return "Kotlin Rocks on ${platformName()}"
}

expect val actorScope: CoroutineScope
expect val mainScope: CoroutineScope