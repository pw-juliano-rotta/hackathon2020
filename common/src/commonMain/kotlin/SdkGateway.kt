package io.payworks.mpos.ui.common

import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow

open abstract class SdkGateway {

    private val broadcastChannel = ConflatedBroadcastChannel<SdkResponse>()

    sealed class SdkRequest {
        data class Login(val user: String, val pass: String) : SdkRequest()
    }

    sealed class SdkResponse {
    }

    abstract fun getTransaction(transactionId: String)
    abstract fun charge(subject: String, amount: Double)

    protected fun offer(response:SdkResponse) {
        broadcastChannel.offer(response)
    }

    fun responses(): Flow<SdkResponse> = broadcastChannel.asFlow()

    fun sendRequest(sdkRequest: SdkRequest) {
//        when (sdkRequest) {
//            is GetTransaction -> getTransaction(sdkRequest.transactionId)
//            is StartTransaction -> charge(sdkRequest.subject, sdkRequest.amount)
//        }
    }
}