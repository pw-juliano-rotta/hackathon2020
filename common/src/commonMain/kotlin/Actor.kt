package io.payworks.mpos.ui.common

import io.payworks.mpos.ui.common.SdkGateway.SdkRequest
import io.payworks.mpos.ui.common.SdkGateway.SdkRequest.*
import io.payworks.mpos.ui.common.SdkGateway.SdkResponse
import io.payworks.mpos.ui.common.SdkGateway.SdkResponse.*
import io.payworks.mpos.ui.common.ViewState.Login
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class Actor(
    private val reducer: StateReducer
) {

    private val flowStates: MutableStateFlow<ViewState> = MutableStateFlow(Login)
    private val states: Flow<ViewState> get() = flowStates
    private val api = Api()

    var onStateChange:((ViewState) -> Unit)? = null

    init {
        states
            .onEach { onStateChange?.invoke(it) }
            .launchIn(mainScope)
    }

    fun sendRequest(request: SdkRequest) {
        println("Actor2 A: $request")

        when (request) {
            is SdkRequest.Login -> {
                actorScope.launch {
                    try {
                        val result = api.auth(request.user, request.pass)

                        println("KTOR API SUCCESS: ${result.status}")
                        Effect.LoginSuccessEffect("barclaycard.terminal", "12a0ea93-850c-45e6-af65-9aa30bf7ad70", "Bq9tC2IFAqYtCeu0zQSJ13JhK37jc2N0").reduce()
                    } catch (e: Exception) {
                        println("KTOR API FAIL: ${e.message}")
                        Effect.LoginFailEffect.reduce()
                    }
                }
                Effect.LoginRequestedEffect(request)
            }
        }.reduce()
    }

    private fun Effect.reduce(): ViewState {
        println("Actor E: $this")
        val oldState = flowStates.value
        val state = reducer.reduce(oldState, this)
        flowStates.value = state
        println("Actor S: $state")
        return state
    }

}