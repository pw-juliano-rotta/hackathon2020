//
//  LoginViewController.swift
//  Paybutton
//
//  Created by Rotta, Juliano on 20/08/2020.
//  Copyright © 2020 Payworks. All rights reserved.
//

import UIKit
import shared

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "segueLogin", sender: self)
//        GitHubApiClient.init(githubUserName: "aaaa", githubPassword: "bbbb")
//            .repos(successCallback: { repos in
//                print(repos)
//                return KotlinUnit()
//        }) { error in
//            print(error)
//            return KotlinUnit()
//        }
    }
}
