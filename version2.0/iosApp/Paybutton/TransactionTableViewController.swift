//
//  TransactionTableViewController.swift
//  Paybutton
//
//  Created by Rotta, Juliano on 20/08/2020.
//  Copyright © 2020 Payworks. All rights reserved.
//

import UIKit
import mpos_ui

class TransactionTableViewController: UITableViewController {

    var transactions: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell", for: indexPath)

        cell.textLabel?.text = "$ 5.00"
        cell.detailTextLabel?.text = transactions[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mposUi = MPUMposUi.initialize(with: .MOCK, merchantIdentifier: "aaa", merchantSecret: "aa")
        
        // TODO: Show transaction summary
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
