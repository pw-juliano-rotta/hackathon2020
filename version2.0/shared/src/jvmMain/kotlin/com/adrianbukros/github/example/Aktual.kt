package com.adrianbukros.github.example

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asCoroutineDispatcher
import java.util.concurrent.Executors

actual fun platformName(): String {
    return "Android"
}

actual val actorScope =
    CoroutineScope(
        Executors.newSingleThreadExecutor().asCoroutineDispatcher() + CoroutineName("actor")
    )

actual val mainScope = CoroutineScope(Dispatchers.Main)