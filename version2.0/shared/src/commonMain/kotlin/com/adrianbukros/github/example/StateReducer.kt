package com.adrianbukros.github.example

import com.adrianbukros.github.example.ViewState.*

class StateReducer {
    fun reduce(viewState:ViewState, effect: Effect) = when(effect) {
        is Effect.LoginRequestedEffect -> LoggingIn
        is Effect.LoginSuccessEffect -> Logged(effect.applicationName, effect.merchantIdentifier, effect.merchantSecretKey)
        is Effect.LoginFailEffect -> Login
    }
}