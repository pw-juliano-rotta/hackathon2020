package com.adrianbukros.github.example

import com.adrianbukros.github.example.SdkGateway.SdkRequest.Login

sealed class ViewState {
    object Login : ViewState()
    object LoggingIn : ViewState()
    data class Logged(val applicationName:String, val merchantIdentifier:String, val merchantSecretKey:String) : ViewState()
    data class Ready(val applicationName:String, val merchantIdentifier:String, val merchantSecretKey:String) : ViewState()
    data class TransactionList(val list: List<Any>) : ViewState()
}

sealed class Effect {
    data class LoginRequestedEffect(val request: Login) : Effect()
    data class LoginSuccessEffect(val applicationName:String, val merchantIdentifier:String, val merchantSecretKey:String) : Effect()
    object LoginFailEffect : Effect()
}