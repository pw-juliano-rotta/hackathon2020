package com.adrianbukros.github.example

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.URLProtocol
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

internal expect val ApplicationDispatcher: CoroutineDispatcher

class Api() {

    private val client = HttpClient()

    suspend fun auth(user:String, pass:String): String {
        return client.post(scheme = "https", host = "services.test.pwtx.info", path = "accounts/authenticate?applicationIdentifier=barclaycard.terminal&deviceIdentifier=payworks.terminal.ios.9c6283096625aacd072c3baa71118b5068d60f949937504d01010779497d806e&profileIdentifier=${user}&profileSecret=${pass}&tokenType=APP&content-type=application/x-www-form-urlencoded")
    }

    suspend fun test(): String {
        return client.get {
            url("https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22")
        }
    }

    fun repos(successCallback: (List<GitHubRepo>) -> Unit, errorCallback: (Exception) -> Unit) {
//        GlobalScope.apply {
//            launch(ApplicationDispatcher) {
//                try {
//                    val reposString = client.get<String> {
//                        url {
//                            protocol = URLProtocol.HTTPS
//                            port = 443
//                            host = "api.github.com"
//                            encodedPath = "user/repos"
//                            header("Authorization", "Basic " + "asd:qwe")
//                            Timber.info { "Sending request to: ${buildString()}" }
//                        }
//                    }
////                    val repos = JSON(strictMode = false).parse(GitHubRepo.serializer().list, reposString)
////                    successCallback(repos)
//                } catch (ex: Exception) {
//                    errorCallback(ex)
//                }
//            }
//        }
    }
}

@Serializable
data class GitHubRepo(
    val name: String,
    @SerialName("html_url")
    val htmlUrl: String
)
