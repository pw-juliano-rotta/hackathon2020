import com.android.build.gradle.internal.dsl.BaseAppModuleExtension
import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.dsl.Coroutines

plugins {
    id("com.android.application")
    kotlin("android")
    id("kotlin-android")
    id("kotlin-android-extensions")
}

android {

    buildToolsVersion = "29.0.3"
    compileSdkVersion(29)
    defaultConfig {
        minSdkVersion(25)
        targetSdkVersion(29)
//        multiDexEnabled = true
        versionCode = 1
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    packagingOptions {
        exclude("META-INF/*")
        exclude("README.txt")
        exclude("LICENSE.txt")
        exclude("asm-license.txt")
    }
}

dependencies {
    implementation("io.payworks:mpos.android.ui:2.49.0")
    implementation("io.payworks:mpos.android.accessories.pax:2.49.0")

    implementation("com.android.support.constraint:constraint-layout:1.1.3")
    implementation("com.android.support.constraint:constraint-layout:1.1.3")
    implementation("io.ktor:ktor-client-json-jvm:1.0.0")
    implementation("io.ktor:ktor-client-okhttp:1.0.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.8")
    implementation("androidx.appcompat:appcompat:1.2.0")
    implementation(kotlin("stdlib-jdk7", KotlinCompilerVersion.VERSION))
    implementation(project(":shared"))
}
