package com.adrianbukros.github.example

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import io.mpos.accessories.AccessoryFamily
import io.mpos.accessories.parameters.AccessoryParameters
import io.mpos.provider.ProviderMode
import io.mpos.ui.shared.MposUi
import com.adrianbukros.github.example.Actor
import com.adrianbukros.github.example.SdkGateway
import com.adrianbukros.github.example.StateReducer
import com.adrianbukros.github.example.ViewState

class LoginActivity : AppCompatActivity() {

    private lateinit var actor: Actor
    private lateinit var username: EditText
    private lateinit var password: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        username = findViewById(R.id.username)
        password = findViewById(R.id.password)
        actor = Actor(StateReducer())
        actor.onStateChange = { updateUi(it) }
    }

    private fun updateUi(viewState: ViewState) {

        println("View state: $viewState")
        when (viewState) {
            is ViewState.Login -> {
            }
            is ViewState.LoggingIn -> {
                // TODO: 21.08.20 SHOW PROGRESS
            }
            is ViewState.Logged -> {
                AcceptAppApplication.mposUi = MposUi.initialize(
                    this,
                    ProviderMode.MOCK,
                    viewState.merchantIdentifier,
                    viewState.merchantSecretKey
//                    "12a0ea93-850c-45e6-af65-9aa30bf7ad70",
//                    "Bq9tC2IFAqYtCeu0zQSJ13JhK37jc2N0"
                )

                val accessoryParameters =
                    AccessoryParameters.Builder(AccessoryFamily.MOCK)
                        .mocked()
                        .build()
                AcceptAppApplication.mposUi.configuration.terminalParameters = accessoryParameters
                startActivity(Intent(this, MainActivity::class.java))

                this@LoginActivity.finish()
            }
            is ViewState.Ready -> {

            }
        }
    }

    fun logIn(view: View) {
        actor.sendRequest(
            SdkGateway.SdkRequest.Login(
                username.text.toString(),
                password.text.toString()
            )
        )
    }
}