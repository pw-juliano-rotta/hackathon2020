package com.adrianbukros.github.example

import android.app.Application
import io.mpos.ui.shared.MposUi

class AcceptAppApplication : Application() {

    companion object {
        lateinit var mposUi: MposUi
    }
}