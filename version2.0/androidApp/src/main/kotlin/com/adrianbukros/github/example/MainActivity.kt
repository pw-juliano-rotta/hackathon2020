package com.adrianbukros.github.example

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.mpos.transactions.Currency
import io.mpos.transactions.parameters.TransactionParameters
import io.mpos.ui.shared.MposUi
import java.math.BigDecimal


class MainActivity : AppCompatActivity() {

    private lateinit var amountTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        amountTextView = findViewById(R.id.inputAmount)
    }

    fun onInputButtonClickListener(view: View) {
        when (view.id) {
            R.id.input0 -> setInput("0")
            R.id.input1 -> setInput("1")
            R.id.input2 -> setInput("2")
            R.id.input3 -> setInput("3")
            R.id.input4 -> setInput("4")
            R.id.input5 -> setInput("5")
            R.id.input6 -> setInput("6")
            R.id.input7 -> setInput("7")
            R.id.input8 -> setInput("8")
            R.id.input9 -> setInput("9")
            R.id.inputDot -> setInput(".")
            R.id.inputCancel -> setInput("C")
        }

    }

    private fun setInput(value: String) {
        val input = amountTextView.text.toString()
        when (value) {
            "1", "2", "3", "4", "5",
            "6", "7", "8", "9" -> amountTextView.text = input + value
            "0" -> {
                if (input.isNotEmpty()) {
                    amountTextView.text = input + value
                }
            }
            "." -> {
                if (!input.contains(".")) {
                    if (input.isEmpty()) {
                        amountTextView.text = "0."
                    } else {
                        amountTextView.text = input + "."
                    }
                }
            }
            "C" -> {
                if (input.isNotEmpty()) {
                    amountTextView.text = input.dropLast(1)
                }
            }
        }
    }

    fun pay(view: View) {
        val transactionParameters = TransactionParameters.Builder()
            .charge(BigDecimal(amountTextView.text.toString()), Currency.EUR)
            .subject("Bouquet of Flowers")
            .customIdentifier("yourReferenceForTheTransaction")
            .build()
        val intent: Intent =
            AcceptAppApplication.mposUi.createTransactionIntent(transactionParameters)
        startActivityForResult(intent, MposUi.REQUEST_CODE_PAYMENT)
        amountTextView.text = ""
    }
}