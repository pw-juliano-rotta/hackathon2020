plugins {
    id("com.android.application")
    kotlin("android")
    id("kotlin-android")
}

android {
    compileSdkVersion(29)
    buildToolsVersion("29.0.3")

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    defaultConfig {
        applicationId = "io.payworks.mpos.ui.android"
        minSdkVersion(22)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }


    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    packagingOptions {
        exclude("META-INF/*")
        exclude("README.txt")
        exclude("LICENSE.txt")
        exclude("asm-license.txt")
    }
}

object Versions {
    const val mposSdk = "2.49.0"
}

dependencies {
    implementation(project(":common"))

    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation("androidx.recyclerview:recyclerview:1.1.0")
    implementation("androidx.appcompat:appcompat:1.2.0")

    implementation("io.payworks:mpos.java.comlinks.tcp:2.49.0")
    implementation("io.payworks:mpos.java.accessories.vipa:2.49.0")
    implementation("io.payworks:mpos.java.comlinks.tcp:2.49.0")
    implementation("io.payworks:mpos.android.comlinks.bluetooth:2.49.0")
    implementation("io.payworks:mpos.java.accessories.miura:2.49.0")

    implementation("io.payworks:mpos.android.ui:${Versions.mposSdk}")
    implementation("io.payworks:mpos.android.accessories.pax:${Versions.mposSdk}")

    implementation("androidx.multidex:multidex:2.0.1")
    implementation("androidx.core:core-ktx:1.3.1")
    implementation("androidx.appcompat:appcompat:1.2.0")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation("com.google.android.material:material:1.2.0")

    androidTestImplementation("junit:junit:4.13")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0")

}