package io.payworks.mpos.ui.android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.payworks.mpos.ui.common.Transaction
import io.payworks.mpos.ui.common.ViewState
import kotlin.collections.ArrayList


class TransactionListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_list)

        val recyclerView: RecyclerView = findViewById(R.id.recycler_view)

        recyclerView.layoutManager = LinearLayoutManager(this)

        recyclerView.adapter = TransactionListAdapter(transactionList())
    }

    private fun transactionList(): ViewState.TransactionList {

        val list = ArrayList<Transaction>()

        list.add(Transaction(1, "10.00", "20/05/2020", 1))
        list.add(Transaction(2, "12.00", "20/05/2020", 0))
        list.add(Transaction(3, "13.00", "20/05/2020", 0))
        list.add(Transaction(4, "14.00", "20/05/2020", 1))
        list.add(Transaction(1, "15.00", "20/05/2020", 1))

        return ViewState.TransactionList(list)
    }
}