package io.payworks.mpos.ui.android

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import io.mpos.accessories.AccessoryFamily
import io.mpos.accessories.parameters.AccessoryParameters
import io.mpos.provider.ProviderMode
import io.mpos.ui.shared.MposUi
import io.payworks.mpos.ui.common.Actor
import io.payworks.mpos.ui.common.SdkGateway
import io.payworks.mpos.ui.common.StateReducer
import io.payworks.mpos.ui.common.ViewState

class LoginActivity : AppCompatActivity() {

    private lateinit var actor: Actor
    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var loading: ProgressBar
    private lateinit var loginButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

        username = findViewById(R.id.username)
        password = findViewById(R.id.password)
        loading = findViewById(R.id.loading)
        loginButton = findViewById(R.id.login_button)

        actor = Actor(StateReducer())
        actor.onStateChange = { updateUi(it) }
    }

    inner class LayoutAnimatiorListener : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {
        }

        override fun onAnimationEnd(animation: Animator?) {
            ObjectAnimator.ofFloat(findViewById(R.id.logo), "translationY", 0f).apply {
                duration = 1000
                start()
            }
        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {
        }
    }

    private fun updateUi(viewState: ViewState) {

        println("View state: $viewState")
        when (viewState) {
            is ViewState.Login -> {
                ObjectAnimator.ofFloat(findViewById(R.id.linearLayout), "translationY", 0f).apply {
                    duration = 1000
                    startDelay = 300
                    start()
                }.addListener(LayoutAnimatiorListener())

            }
            is ViewState.LoggingIn -> {
                loading.visibility = View.VISIBLE
                loginButton.isEnabled = false
            }
            is ViewState.Logged -> {
                loading.visibility = View.GONE
                AcceptAppApplication.mposUi = MposUi.initialize(
                    this,
                    ProviderMode.MOCK,
                    viewState.merchantIdentifier,
                    viewState.merchantSecretKey
//                    "12a0ea93-850c-45e6-af65-9aa30bf7ad70",
//                    "Bq9tC2IFAqYtCeu0zQSJ13JhK37jc2N0"
                )

                val accessoryParameters =
                    AccessoryParameters.Builder(AccessoryFamily.MOCK)
                        .mocked()
                        .build()
                AcceptAppApplication.mposUi.configuration.terminalParameters = accessoryParameters
                startActivity(Intent(this, MainActivity::class.java))

                this@LoginActivity.finish()
            }
            is ViewState.Ready -> {

            }
        }
    }

    fun logIn(view: View) {
        actor.sendRequest(
            SdkGateway.SdkRequest.Login(
                username.text.toString(),
                password.text.toString()
            )
        )
    }
}