package io.payworks.mpos.ui.android

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.payworks.mpos.ui.common.ViewState


class TransactionListAdapter(private val transactionList: ViewState.TransactionList) :
    RecyclerView.Adapter<TransactionListAdapter.ContactHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
        return ContactHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.transaction_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int = transactionList.list.size

    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        holder.amount.text = transactionList.list[position].amount
        holder.date.text = transactionList.list[position].date

        when (transactionList.list[position].cardType) {
            1 -> holder.cardLogo.setImageResource(R.drawable.mpu_visacard_image)
            2 -> holder.cardLogo.setImageResource(R.drawable.mpu_mastercard_image)
            3 -> holder.cardLogo.setImageResource(R.drawable.mpu_american_express_image)
            4 -> holder.cardLogo.setImageResource(R.drawable.mpu_alipay_image)
        }

        when (transactionList.list[position].status) {
            0 -> {
                holder.status.text = "R"
                holder.status.setTextColor(holder.itemView.resources.getColor(android.R.color.holo_red_dark))
            }
            1 -> {
                holder.status.text = "A"
                holder.status.setTextColor(holder.itemView.resources.getColor(android.R.color.holo_green_dark))
            }
        }

        if (position % 2 == 0) {
            holder.itemView.setBackgroundColor(holder.itemView.resources.getColor(R.color.blue_bc2))
            holder.amount.setTextColor(holder.itemView.resources.getColor(android.R.color.white))
            holder.date.setTextColor(holder.itemView.resources.getColor(android.R.color.white))
        } else {
            holder.itemView.setBackgroundColor(holder.itemView.resources.getColor(android.R.color.white))
            holder.amount.setTextColor(holder.itemView.resources.getColor(R.color.blue_bc2))
            holder.date.setTextColor(holder.itemView.resources.getColor(R.color.blue_bc2))
        }
    }

    class ContactHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardLogo: ImageView = view.findViewById(R.id.card_logo)
        var amount: TextView = view.findViewById(R.id.amount)
        var date: TextView = view.findViewById(R.id.date)
        var status: TextView = view.findViewById(R.id.status)
    }
}